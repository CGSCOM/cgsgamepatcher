﻿using Oxide.Patcher.Patching;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Oxide.Patcher
{
    static class Program
    {
        // defines for commandline output
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);

        private const int ATTACH_PARENT_PROCESS = -1;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            AppDomain.CurrentDomain.AssemblyResolve += (sender, args1) =>
            {
                String resourceName = "Oxide.Patcher.Dependencies." +
                   new AssemblyName(args1.Name).Name + ".dll";
                if (resourceName.Contains("resources.dll"))
                {
                    return null;
                }
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                {
                    Byte[] assemblyData = new Byte[stream.Length];

                    stream.Read(assemblyData, 0, assemblyData.Length);

                    return Assembly.Load(assemblyData);
                }
            };

            if (args.Length == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new PatcherForm());
            }
            else
            {
                RebuildArgs(args, out var oargs);
                args = oargs;

                bool console = false;
                string filename = "";
                bool unflagAll = false;
                string targetOverride = "";

                for(var i = 0; i < args.Length; i++)
                {
                    switch(args[i].ToLower())
                    {
                        case "-u":
                        case "-unflag":
                            unflagAll = true;
                            continue;

                        case "-console":
                        case "-c":
                            console = true;
                            AttachConsole(ATTACH_PARENT_PROCESS);
                            continue;

                        case "-project":
                        case "-p":
                            {
                                if (args.Length == i)
                                {
                                    var files = Directory.GetFiles(Environment.CurrentDirectory, "*.cgspj", SearchOption.TopDirectoryOnly);
                                    filename = files.FirstOrDefault();
                                    continue;
                                }

                                if (args[i + 1].StartsWith("-"))
                                {
                                    var files = Directory.GetFiles(Environment.CurrentDirectory, "*.cgspj", SearchOption.TopDirectoryOnly);
                                    filename = files.FirstOrDefault();
                                    continue;
                                }
                                i++;
                                filename = args[i];
                            }
                        continue;

                        case "-d":
                        case "-dir":
                            {
                                if (args.Length == i)
                                {
                                    targetOverride = Environment.CurrentDirectory;
                                    continue;
                                }

                                if (args[i + 1].StartsWith("-"))
                                {
                                    targetOverride = Environment.CurrentDirectory;
                                    continue;
                                }
                                i++;
                                targetOverride = args[i];
                                
                            }
                            continue;

                        default:

                            continue;


                    }
                }
                
                if (!Directory.Exists(targetOverride) && targetOverride != "")
                {
                    if (console)
                    {
                        if (Console.IsErrorRedirected)
                        {
                            Console.Error.WriteLine($"Directory Override does not exist: {targetOverride}");
                            return;
                        }

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Directory Override does not exist: {targetOverride}");
                        Console.ResetColor();
                    }
                    else
                    {
                        MessageBox.Show(targetOverride + " does not exist!", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    return;
                }

                if (!File.Exists(filename))
                {
                    if (console)
                    {
                        if (Console.IsErrorRedirected)
                        {
                            Console.Error.WriteLine($"Project File: {filename} does not exist!");
                            return;
                        }

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Project File: {filename} does not exist!");
                        Console.ResetColor();
                    }
                    else
                    {
                        MessageBox.Show(filename + " does not exist!", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    return;
                }

                Project PatchProject = null;
                if (targetOverride == "")
                {
                    PatchProject = Project.Load(filename);
                }
                else
                {
                    PatchProject = Project.Load(filename, targetOverride);

                    if (Console.IsOutputRedirected)
                    {
                        Console.Out.WriteLine("Project Loaded With Override " + PatchProject.TargetDirectory);
                    }
                    else
                    {
                        Console.WriteLine("Project Loaded With Override " + PatchProject.TargetDirectory);
                    }
                }
                if (unflagAll)
                {
                    unflag(PatchProject, filename, console);
                }
                if (!console)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new PatcherForm(filename));
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Preparing to Patch Files for Project {PatchProject.Name}");

                    Patching.Patcher patcher = new Patching.Patcher(PatchProject, true);
                    patcher.Patch();
                    var hooks = PatchProject.Manifests.SelectMany(m => m.Hooks);
                    var overrides = PatchProject.Manifests.SelectMany(m => m.Modifiers);
                    var fields = PatchProject.Manifests.SelectMany(m => m.Fields);
                    Console.WriteLine($"Finished Patching {Enumerable.Where(hooks, (p) => !p.Flagged).Count()}/{hooks.Count()} Hooks {Enumerable.Where(overrides, (p => !p.Flagged)).Count()}/{overrides.Count()} Modifiers {Enumerable.Where(fields, (p => !p.Flagged)).Count()}/{fields.Count()} Fields");
                    Console.ResetColor();
                }
            }
        }

        private static void unflag(Project project, string filename, bool console)
        {
            bool updated = false;
            foreach (var hook in project.Manifests.SelectMany((m) => m.Hooks))
            {
                if (hook.Flagged)
                {
                    hook.Flagged = false;
                    updated = true;
                    if (console)
                    {
                        Console.WriteLine("Hook " + hook.HookName + " has been unflagged.");
                    }
                }
            }
            if (updated)
            {
                project.Save(filename);
            }
        }

        private static void RebuildArgs(string[] args, out string[] outArgs)
        {
            outArgs = null;
            if (args == null) return;
            if (args.Length == 0) return;

            var list = new System.Collections.Generic.List<string>();

            bool BuildingStr = false;
            var sb = new System.Text.StringBuilder();
            foreach (var arg in args.ToList())
            {
                if (arg.StartsWith("-"))
                {
                    list.Add(arg);
                    continue;
                }

                if (arg.StartsWith("\""))
                {
                    BuildingStr = true;
                    sb = new System.Text.StringBuilder();
                    sb.Append(arg.TrimStart('"'));
                    sb.Append(" ");
                    continue;
                }

                if (arg.EndsWith("\""))
                {
                    BuildingStr = false;
                    sb.Append(arg.TrimEnd('"'));
                    list.Add(sb.ToString().Trim());
                    sb = null;
                    continue;
                }

                if (BuildingStr)
                {
                    sb.Append($"{arg} ");
                    continue;
                }

                list.Add(arg);
            }

            outArgs = list.ToArray();
        }
    }
}