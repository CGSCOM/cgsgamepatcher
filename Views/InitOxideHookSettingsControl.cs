﻿using System;

using Oxide.Patcher.Hooks;

namespace Oxide.Patcher.Views
{
    public partial class InitOxideHookSettingsControl : HookSettingsControl
    {
        private bool ignorechanges;

        public InitOxideHookSettingsControl()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var hook = Hook;

            ignorechanges = true;
            injectionindex.Value = (hook is InitCGS) ? (hook as InitCGS).InjectionIndex : (hook as ShutdownCGS).InjectionIndex;
            ignorechanges = false;
        }

        private void injectionindex_ValueChanged(object sender, EventArgs e)
        {
            if (ignorechanges) return;

            var hook = Hook;
            if (hook is InitCGS)
            {
                (hook as InitCGS).InjectionIndex = (int)injectionindex.Value;
            }
            else
            {
                (hook as ShutdownCGS).InjectionIndex = (int)injectionindex.Value;
            }
            NotifyChanges();
        }
    }
}
