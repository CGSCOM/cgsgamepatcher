﻿using System;
using System.Linq;
using System.Windows.Forms;

using System.IO;

namespace Oxide.Patcher
{
    public partial class ProjectSettingsControl : UserControl
    {
        public Project ProjectObject { get; set; }

        public string ProjectFilename { get; set; }

        public ProjectSettingsControl()
        {
            InitializeComponent();
        }

        private void ProjectSettingsControl_Load(object sender, EventArgs e)
        {
            nametextbox.Text = ProjectObject.Name;
            directorytextbox.Text = ProjectObject.TargetDirectory;
            filenametextbox.Text = ProjectFilename;
            selectdirectorybutton.Enabled = true;
            selectdirectorybutton.Show();
            selectdirectorybutton.Refresh();
        }

        private void savebutton_Click(object sender, EventArgs e)
        {
            // Verify
            if (!Directory.Exists(directorytextbox.Text))
            {
                MessageBox.Show(this, "The target directory is invalid.", "CGS Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Directory.Exists(Path.GetDirectoryName(filenametextbox.Text)))
            {
                MessageBox.Show(this, "The filename is invalid.", "CGS Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (nametextbox.TextLength == 0)
            {
                MessageBox.Show(this, "The project name is invalid.", "CGS Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Save
            ProjectObject.Name = nametextbox.Text;
            ProjectObject.TargetDirectory = directorytextbox.Text;
            ProjectObject.Save(ProjectFilename);
            PatcherForm.MainForm.RepopulateTree();
            PatcherForm.MainForm.Refresh();
        }

        private void selectdirectorybutton_Click(object sender, EventArgs e)
        {
            using (var diag = new FolderBrowserDialog())
            {
                diag.ShowNewFolderButton = false;
                if (!string.IsNullOrWhiteSpace(ProjectObject.TargetDirectory))
                {
                    diag.SelectedPath = ProjectObject.TargetDirectory;
                }
                var result = diag.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(diag.SelectedPath))
                {
                    var files = Directory.GetFiles(diag.SelectedPath, "*dll").Concat(Directory.GetFiles(diag.SelectedPath, "*exe"));

                    if (files != null && files.Count() > 0)
                    {
                        ProjectObject.TargetDirectory = diag.SelectedPath;
                        return;
                    }

                    var pop = MessageBox.Show("No compatable files present in selected Directory", "CGSGamePatcher", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                    if (pop == DialogResult.Retry)
                    {
                        selectdirectorybutton_Click(sender, e);
                        return;
                    }

                    return;
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
                else
                {
                    var pop = MessageBox.Show("Invalid Path Selected", "CGSGamePatcher", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);

                    if (pop == DialogResult.Retry)
                    {
                        selectdirectorybutton_Click(sender, e);
                        return;
                    }

                    return;
                }
            }
        }
    }
}